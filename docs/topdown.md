
## Controller

The player controller should include the following components.

```haxe
import dex.wrappers.DexScriptWithComponents;
import dex.wrappers.ScriptWithComponents.ScriptWithComponentsProperties;

typedef PlayerProperties = ScriptWithComponentsProperties &
{
    // additional properties
}


class Player extends DexScriptWithComponents<PlayerProperties>
{
    override function init(self: PlayerProperties)
    {
        go.acquireInputFocus();

        self.components = [
            // a component Input2D or InputWithCamera2D
            new InputWithCamera2D(),
            // a raycast collider
            new RaycastCollider(
                [ CollisionGroups.map ],  // the groups that the character should collide with
                new Vector2(0, 4),        // the offset of the collider's center
                4                         // the radius of the collider
            ),
            // a component TopdownMover with configured speed
            new TopdownMover(150),
        ];

        // init must be called after the components list has been filled
        super.init(self);
    }
}
```


## Camera

```haxe
typedef CameraProperties = dex.scripts.Camera.CameraProperties &
{
}

class Camera extends dex.scripts.Camera<CameraProperties>
{
    /**
     * Nothing to implement by default.
     * Simply extending the base Camera class so that a script is generated.
     */
}
```

Then a game object should be added to the scene with `Camera.script` and a camera object with id `#camera`.

The camera can be controller through the following properties:

- **`zoom`:**`Float` - higher value zooms in, lower value zooms out
- **`center`:**`Vector3` - the world position that should be in the center of the camera
- **`follow`:**`Hash` - optional object id that the camera should follow and keep at its center; if specified then the `center` property should not be touched by the user
- **`followOffsetX`, `followOffsetY`:**`Float` - parameters to specify an offset that the camera's center should have from the followed object's position
- **`followLerpX`, `followLerpY`:**`Float` - parameters to control how *smoothly* the `follow` object is tracked
    - These are used as interpolation parameters and should be in the range `[0, 1]`.
    - A value of `1.0` will result in the camera always pointing exactly at the followed object, while lower values will cause the camera to move to the object over some time.
