
A component system has been implemented to compose complex objects and to reuse functionality.

For a script to utilize components, it needs to extend either `ScriptWithComponents` or `DexScriptWithComponents`, and generally follow the structure shown below.

```haxe
import dex.wrappers.ScriptWithComponents;

typedef MyScriptProperties = ScriptWithComponentsProperties &
{
    // the script's own properties
}

class MyScript extends ScriptWithComponents<MyScriptProperties>
{
    override function init(self: MyScriptProperties)
    {
        self.components = [
            // create a list with all needed components
        ];

        // init must be called after the components list has been filled
        super.init(self);
    }
}
```

Specific components can be retrieved from the list either by index or by type.

```haxe
// easier to write, but slower
var input: Input2D = self.components[ Input2D ];

// much faster, but requires an eplicit cast and assumes that we keep track of the index
var mover: Mover = cast self.components[ 2 ];
```

Components can be created by extending the `ScriptComponent` class.
The component can then implement any functionality by overriding any of the callback methods, same as in a script.

```haxe
class MyComponent extends ScriptComponent
{
    override function init() { }

    override function update(dt: Float) { }

    override function onInput(actionId: Hash, action: ScriptOnInputAction): Bool { }

    override function onMessage<TMessage>(messageId: Message<TMessage>, message: TMessage, sender: Url) { }
}
```


## Useful components

Some generally useful components come bundled with dex.

- **`AnimationCursorTrigger`**
    - This component will monitor the given sprite's animation cursor, and trigger callbacks and configured thresholds.
- **`AnimationState`**
    - Handy component for controlling a sprite animation. Supports controlling multiple sprites at once, locking the current animation, queueing animations etc.
- **`Input2D`**
    - Utility for getting 2D (up-down-left-right) and mouse input from the player.
- **`InputWithCamera2D`**
    - Extension of `Input2D`, that can be used when the dex `Camera` script is also used in a scene. It just calculates and keeps the mouse world position once per frame.
- **`RangeTrigger`**
    - On an object with a trigger collider, this simple component keeps a list with all other objects that are currently overlapping with this one.
- **`RaycastCollider`**
    - Can be used to separate from and/or detect collisions with other static or kinematic colliders, using raycasting.
    - In some cases, this can be advantageous to the traditional kinematic-separation way of keeping objects apart.


### RaycastCollider

This component works by casting 8 radial rays from the object's position, and measuring its available distance from obstacles.

```haxe
// configure the collider script component
var collider: RaycastCollider = new RaycastCollider(
    [hash("walls"), hash("trees")], // the object should not overlap with walls and trees
    new Vector2(0, 0),              // offset from the object's position - in this case place the collider at the center
    16                              // radius of the collider
);

// update it once per frame
collider.update(dt);
```

Note that the information stored in the collider is only valid at the time when `collider.update(dt)` gets called, since that is when the rays are cast.
If the object is moved, then the collider should not be used again.
Therefore, the order of operations should be considered carefully.

To detect collisions, simply use the `hasOverlap()` method.

```haxe
if (collider.hasOverlap())
{
    // there is an overlap currently ongoing
}
```

If the object is moving though, a better approach is to prevent overlaps entirely by constraining the movement vector.
This approach ensures that overlaps won't occur, and avoids separating objects entirely.

```haxe
// suppose we won't to move the object in this frame
var moveVec: Vector2 = speed * direction * dt;

// before moving the object, use the collider to limit the movement vector
collider.constrainMove(moveVec);

// then use it to move the object
go.move(moveVec);
```

Note that the `constrainMove()` method may reverse the movement on some axis, if it is necessary to separate from another collider.
For example, if there is already an overlap of `3px` on the right side of the object, then if `moveVec.x` is larger than `-3`, it will be set to `-3`.

To detect collisions while using the `constainMove` method, use the `willCollide` methods before it.

```haxe
if (collider.willCollideBottom(moveVec))
{
    // the object is touching the ground
    onGrounded();
}

// then do the rest
collider.constrainMove(moveVec);
go.move(moveVec);
```
