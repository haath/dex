package dex.types;

enum abstract CameraShake(Int)
{
    var Sine;
    var SineDiminishing;
    var Random;
    var RandomDiminishing;
    var RandomPerlin;
    var Square;
    var SquareDiminishing;
}
