package dex.gui;

import defold.types.Hash;


typedef ChatBubbleTextOptions =
{
    var id: Hash;

    var scroll: Bool;
    var addedHeightPerExtraLine: Int;
}
