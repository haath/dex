package dex.util.rng;

enum DiceRoll
{
    /** Sum the values of all dice rolls. */
    Sum;

    /** Keep only the highest roll value. */
    KeepHighest;

    /** Keep only the lowest roll value. */
    KeepLowest;
}
