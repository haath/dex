package dex.util.rng;

enum abstract DieSides(Int) to Int from Int
{
    var D4 = 4;
    var D6 = 6;
    var D8 = 8;
    var D10 = 10;
    var D12 = 12;
    var D20 = 20;
    var D30 = 30;
    var D100 = 100;
}
