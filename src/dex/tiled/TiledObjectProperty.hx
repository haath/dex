package dex.tiled;

typedef TiledObjectProperty =
{
    var name: String;
    var type: String;
    var value: String;
}
