package dex.topdown.hashes;

@:build(defold.support.HashBuilder.build())
class TopdownAnimations
{
    var idle;
    var walk;
    var run;
}
